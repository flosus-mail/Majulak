﻿/// <reference path="Game.ts" />
class Renderer {

    visibleVertical: number = 10;
    visibleHorizontal: number = 10;

    /**
     * All tiles. [X][Y]
     */
    rendertiles: RenderTile[][];

    assetRegistry: AssetRegistry;

    /**
     * This variable is used to store the mainDiv HTMLElement and the Game object.
     */
    constructor(public mainDiv: HTMLElement, public game: Game) {

        this.rendertiles = new Array<Array<RenderTile>>();

        for (var x = 0; x < 21; x++) {
            var tmpInner: RenderTile[] = new Array<RenderTile>();
            var row = document.createElement('div');
            row.setAttribute("style", "display: inline-block;");
            for (var y = 0; y < 21; y++) {
                var rt: RenderTile = new RenderTile();
                rt.element = document.createElement('div');
                tmpInner.push(rt);
                row.appendChild(rt.element);
            }
            mainDiv.appendChild(row);
            this.rendertiles.push(tmpInner);
        }

        this.assetRegistry = new AssetRegistry();
        this.assetRegistry.initialise();
    }

    start() {
        window.requestAnimationFrame(this.renderNextStep);
    }

    renderNextStep = (stept): void => {
        var xRenderTileCount = 0;
        for (var xCount: number = this.game.player.position.x - this.visibleHorizontal; xCount <= this.game.player.position.x + this.visibleHorizontal; xCount++) {
            var yRenderTileCount = 0;
            // this.mainDiv.innerHTML += "<div>"
            for (var yCount: number = this.game.player.position.y - this.visibleVertical; yCount <= this.game.player.position.y + this.visibleVertical; yCount++) {

                // Get tile
                var tileToRender: Tile = this.game.currentMap.getTileAt(xCount, yCount);

                // Check structure

                // render if changed
                if (tileToRender.isDirty || this.rendertiles[xRenderTileCount][yRenderTileCount].tile != tileToRender) {
                    this.rendertiles[xRenderTileCount][yRenderTileCount].tile = tileToRender;
                    this.renderBackground(this.rendertiles[xRenderTileCount][yRenderTileCount]);
                    this.renderForeground(this.rendertiles[xRenderTileCount][yRenderTileCount]);
                    // TODO render entity?
                    tileToRender.isDirty = false;
                }
                yRenderTileCount++;
            }
            // this.mainDiv.innerHTML += "</ div>"
            xRenderTileCount++;
        }
        window.requestAnimationFrame(this.renderNextStep);
    }

    renderBackground = (tileToRender: RenderTile): void => {
        tileToRender.element.setAttribute("style", "border-style: solid; border-width: 5px; height: 20px; width: 20px;");
        tileToRender.element.innerText = (tileToRender.tile.position.x + tileToRender.tile.position.y) + "";
        //this.mainDiv.innerHTML += "<div style=\"width:20px; height:20px; background-color: " + tileToRender.background + " \" />";
    }
    renderForeground = (tileToRender: RenderTile): void => {
        //this.mainDiv.innerHTML = "<div style=\"width:20px; height:20px; background-color: silver \" />";
    }
}

class RenderTile {
    tile: Tile = null;
    element: HTMLElement = null;
}

class AssetRegistry {
    assets: {[id: string]: string} = {};
    initialise = (): void => {
        var request = new XMLHttpRequest();
        request.onload = this.onRequestFinished;
        request.open("get", "./assets/assets.json", true);
        request.send();
    }

    private onRequestFinished = (something): void => {
        //var fileValues = JSON.parse
        console.log("onRequestFinished");
        console.log(something);
        //this.assets
    }
}
