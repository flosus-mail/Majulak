﻿class Game {
    currentMap: Map;
    player: Player;
}

class Map {
    /**
     * All tiles. [X][Y]
     */
    tiles: Tile[][];

    constructor(public defaultTile: Tile) {
        if (this.defaultTile == null) {
            this.defaultTile = new Tile();
            this.defaultTile.position = null;
            this.defaultTile.isSeeThrough = false;
            this.defaultTile.isWalkable = false;
            this.defaultTile.background = 'black';
            this.defaultTile.foreground = null;
        }

        this.tiles = new Array<Array<Tile>>();
        for (var x = 0; x < 1000; x++) {
            var tmpInner: Tile[] = new Array<Tile>();
            for (var y = 0; y < 1000; y++) {
                tmpInner.push(this.defaultTile.clone());
                tmpInner[y].position = new TilePosition(x, y);
            }
            this.tiles.push(tmpInner);
        }

    }

    getTileAt = (x: number, y: number): Tile => {
        if (this.tiles[x][y] == null) {
            return this.defaultTile.clone();
        }
        return this.tiles[x][y];
    }

    addTile = (tile: Tile): void => {
        this.tiles[tile.position.x][tile.position.y] = tile;
    }
}

class Tile {
    position: TilePosition;
    isWalkable: boolean;
    isSeeThrough: boolean;
    background: string;
    foreground: string;
    isDirty: boolean = true;

    clone = (): Tile => {
        var c: Tile = new Tile;
        c.isSeeThrough = this.isSeeThrough;
        c.isWalkable = this.isWalkable;
        c.background = this.background;
        c.foreground = this.foreground;
        c.isDirty = this.isDirty;
        return c;
    }
}

/**
 * The coordinates of a tile, player or similar.
 */
class TilePosition {
    constructor(public x: number, public y: number) { }
}

class Player {
    position: TilePosition;
    constructor(public playername: string) { }
}
