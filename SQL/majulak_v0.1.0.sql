--liquibase formatted sql

--changeset flosus:v0.1.0_CREATE_majulak_system.player
CREATE TABLE majulak_system.player (
    id          INTEGER     PRIMARY KEY NOT NULL,
    name        TEXT                    NOT NULL,
    last_login  INTEGER                 NOT NULL
);
--rollback DROP TABLE majulak_system.player;

--changeset flosus:v0.1.0_CREATE_majulak_game.character
CREATE TABLE majulak_game.character (
    id          INTEGER     PRIMARY KEY NOT NULL,
    name        TEXT                    NOT NULL,
    last_login  INTEGER                 NOT NULL,
    xp          INTEGER                 NOT NULL,
    level       INTEGER                 NOT NULL,
    gold        INTEGER                 NOT NULL
);
--rollback DROP TABLE majulak_system.character;
