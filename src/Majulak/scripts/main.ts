﻿/// <reference path="Renderer.ts" />
/// <reference path="Game.ts" />

class EventHandler {

    constructor() {
        this.registerKeyPress('gameMainDiv');
    }

    registerKeyPress = (input: string): void => {
        document.addEventListener('keypress', this.keyPressEvent);
    }

    keyPressEvent = (e: KeyboardEvent) => {
        console.log(e);
    }
}

// Setup game
var game: Game = new Game();
game.currentMap = new Map(null);
game.player = new Player("Flosus");
game.player.position = new TilePosition(20, 20);

// Initialising event handler
var eventHandler = new EventHandler();

// Setup renderer
var renderer = new Renderer(document.getElementById('gameMainDiv'), game);

// Let's start this.
renderer.start();

