﻿/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/

var gulp = require('gulp'),
    ts = require('gulp-typescript');

var paths = {
    tsSource: './scripts/*.ts',
    tsOutput: './wwwroot/',
    tsDef: 'definitions/'
};


var tsProject = ts.createProject('./scripts/tsconfig.json');

gulp.task('default', function () {
    // place code for your default task here
});

gulp.task('compile-typescript', function (done) {
    var tsResult = gulp.src([
       "scripts/*.ts"
    ])
     .pipe(ts(tsProject), undefined, ts.reporter.fullReporter());
    return tsResult.js.pipe(gulp.dest(paths.tsOutput));
});