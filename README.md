# Majulak

A small RPG for ASP.Net.

# Dependencies

The database setup uses [liquibase](http://www.liquibase.org/index.html).
The database itself is a [SQLite](https://www.sqlite.org/) database.
The SQLite-JDBC for liquibase can be found [here](https://github.com/xerial/sqlite-jdbc). 

# Setup

You need to create two databases.

    sqlite3 majulak_system.db
    sqlite3 majulak_game.db

To build and run the project use:

    dotnet restore
    dotnet run


Database setup will be added in the future.

